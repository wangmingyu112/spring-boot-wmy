package com.itheima.controller;


import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @Value("${server.port}")
    private String port ;


    @GetMapping("findUserById/{id}")
    public User findUserById(@PathVariable Integer id){

        if(id==1){
            throw new RuntimeException("11");
        }
        User user = userService.findById(id);
        user.setNote("生产者端口号是："+port);
        return user;
    }
}

