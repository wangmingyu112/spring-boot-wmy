package com.itheima.controller;


import com.itheima.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
@RequestMapping("consumer")
public class UserController {

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private DiscoveryClient discoveryClient;


        @RequestMapping("findUserById/{id}")
    public User findUserById(@PathVariable Integer id){
//     String url =   discoveryClient.getInstances("user-service").get(0).getUri()+"/user/findUserById/" + id;
//        return restTemplate.getForObject(url,User.class);

        String url ="http://user-service/user/findUserById/" + id;

      return   restTemplate.getForObject(url,User.class);

    }

    @GetMapping("getInstances")
    public List<ServiceInstance> getInstances(){

        List<ServiceInstance> instances = discoveryClient.getInstances("user-service");
        return instances;
    }
}
