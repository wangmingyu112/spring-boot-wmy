package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class SpringBoot2ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot2ConsumerApplication.class, args);
    }


    @Bean
    @LoadBalanced //使用ribbon进行负载调用
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
