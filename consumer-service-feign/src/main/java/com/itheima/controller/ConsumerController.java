package com.itheima.controller;


import com.itheima.pojo.User;
import com.itheima.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("consumer")
public class ConsumerController {

    @Autowired
    ConsumerService consumerService;

    @GetMapping("/findUserById/{id}")
    public User findUserById(@PathVariable Integer id){

      return consumerService.findUserById(id);
    }
}
