package com.itheima.service;

import com.itheima.pojo.User;
import org.springframework.stereotype.Component;

@Component
public class ConsumerServiceImpl implements ConsumerService {


    @Override
    public User findUserById(Integer id) {
        User user = new User();
        user.setId(id);
        user.setNote("服务繁忙，请您稍后再试...");
        return user;
    }
}
