package com.itheima.service;

import com.itheima.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value="user-service",fallback = ConsumerServiceImpl.class)
public interface ConsumerService {


    @GetMapping("user/findUserById/{id}")
    User findUserById(@PathVariable("id") Integer id);
}
