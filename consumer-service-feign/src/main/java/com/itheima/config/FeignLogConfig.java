package com.itheima.config;


import feign.Logger;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignLogConfig {

    public Logger.Level confidLog(){
        return Logger.Level.FULL;
    }
}
